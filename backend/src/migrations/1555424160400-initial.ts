import {MigrationInterface, QueryRunner} from "typeorm";

export class initial1555424160400 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("CREATE TABLE `asset` (`id` varchar(255) NOT NULL, `url` varchar(500) NOT NULL, `type` int NOT NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("CREATE TABLE `user` (`id` varchar(255) NOT NULL, `fullName` varchar(100) NOT NULL, `email` varchar(100) NOT NULL, `pictureId` varchar(255) NULL, `pdfId` varchar(255) NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_7478a15985dbfa32ed5fc77a7a1` FOREIGN KEY (`pictureId`) REFERENCES `asset`(`id`)");
        await queryRunner.query("ALTER TABLE `user` ADD CONSTRAINT `FK_9e582b026f19149b45011820a94` FOREIGN KEY (`pdfId`) REFERENCES `asset`(`id`)");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_9e582b026f19149b45011820a94`");
        await queryRunner.query("ALTER TABLE `user` DROP FOREIGN KEY `FK_7478a15985dbfa32ed5fc77a7a1`");
        await queryRunner.query("DROP TABLE `user`");
        await queryRunner.query("DROP TABLE `asset`");
    }

}
