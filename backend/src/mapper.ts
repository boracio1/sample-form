import "automapper-ts";

import { UserCreateModel } from "~/models/user"
import { User } from "~/entities/user"

export function initializeAutoMapper() {
  automapper
    .createMap(UserCreateModel.key, User.key)
    .convertToType(User);
}