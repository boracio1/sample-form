import { static as Static, Express } from "express";
import * as SwaggerUI from "swagger-ui-dist";
import * as SwaggerJSDoc from "swagger-jsdoc";

// tslint:disable-next-line:no-var-requires
const packageJson = require("../package.json");

/**
 * Configures the swagger documentation ui
 * @param server Express server
 */
export function createSwaggerDocs(server: Express) {
  const swaggerDocument = SwaggerJSDoc({
    swaggerDefinition: {
      info: {
        title: "Pipedrive Registration form",
        version: packageJson.version
      }
    },
    apis: [
      `${__dirname}/controllers/*.ts`,
      `${__dirname}/swagger.globals.yaml`,
      // We use this for definitions
      `${__dirname}/models/*.ts`,
    ]
  });

  server.get("/api-docs/swagger.json", (req, res) => res.send(swaggerDocument));
  server.get("/api-docs", (req, res) => res.sendFile(`${__dirname}/swagger.html`));
  server.use("/api-docs", Static(SwaggerUI.absolutePath()));
}