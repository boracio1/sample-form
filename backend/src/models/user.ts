import { IsEmail, Length, IsString } from "class-validator";

/**
 * @swagger
 * definitions:
 *  UserCreateModel:
 *   type: object
 *   properties:
 *    email:
 *     type: string
 *     format: email
 *     required: true
 *    fullName:
 *     type: string
 *     required: true
 *     maxLength: 100
 *     minLength: 3
 *    picture:
 *     type: file
 *     required: true
 *    pdf:
 *     type: file
 *     required: true
 */
export class UserCreateModel {
  public static key = "UserCreateModel";

  @IsEmail()
  public email: string = null;

  @IsString()
  @Length(3, 100)
  public fullName: string = null;
}
