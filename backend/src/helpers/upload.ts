import * as fs from "fs";

export class UploadFileHelper {
  public static options = {
    fileFilter: (req, file: Express.Multer.File, next) => {
      let maxFileSize = 5e6; // 5MB
      const size = file.size || req.headers["content-length"] || Infinity;
      const wordMime = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      switch (file.mimetype) {
        case "application/pdf":
        case wordMime:
          maxFileSize = 20e6; // 20MB
      }
      if (size > maxFileSize) {
        next(null, false);
        return;
      }
      next(null, true);
    },
    limits: {
      fileSize: 100e6
    }
  };

  /**
   * 
   * @param file Buffer to upload
   * @param name Name of uploaded file
   * @param userId Id of a new user 
   */
  public static async uploadAsset(file: Buffer, userId: string, name: string): Promise<string> {
    // Write file to local system
    const assetUrl = `/tmp/files/${userId}/`;
    // Check if folders exist and create one if not
    if (!fs.existsSync("/tmp/files/"))
      fs.mkdirSync("/tmp/files/");
    if (!fs.existsSync(assetUrl))
      fs.mkdirSync(assetUrl);
    const stream = fs.createWriteStream(`${assetUrl}${name}`);
    stream.write(file);
    stream.end();

    return assetUrl;
  }
}