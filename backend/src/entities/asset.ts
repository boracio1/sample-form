import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

export enum AssestType {
  None = 0,
  PDF = 1,
  Picture = 2
}

@Entity()
export class Asset {
  public static key = "Asset";

  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({ length: 500 })
  public url: string = null;

  @Column()
  public type: AssestType = AssestType.None;
}