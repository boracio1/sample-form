import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { Asset } from "./asset";

@Entity()
export class User {
  public static key = "User";

  @PrimaryGeneratedColumn("uuid")
  public id: string;

  @Column({ length: 100 })
  public fullName: string = null;

  @Column({ length: 100 })
  public email: string = null;

  @ManyToOne(() => Asset, a => a.id, { eager: true })
  public picture: Asset;

  @ManyToOne(() => Asset, a => a.id, { eager: true })
  public pdf: Asset;
}