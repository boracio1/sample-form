import "reflect-metadata";

import { Express } from "express";
import * as TypeORM from "typeorm";
import * as express from "express";
import * as Routing from "routing-controllers";
import * as cors from "cors";
import { MysqlConnectionOptions } from "typeorm/driver/mysql/MysqlConnectionOptions";
import { createSwaggerDocs } from "./swagger";
import { initializeAutoMapper } from "./mapper";


async function init() {
  // Initialize DB
  const db = new Promise(async (resolve, reject) => {
    let options = await TypeORM.getConnectionOptions() as MysqlConnectionOptions;
    options = {
      ...options,
      maxQueryExecutionTime: 2500,
      timezone: "UTC"
    };
    await TypeORM.createConnection(options);

    resolve();
  }).catch((error) => {
    console.log (error);
  });

  // Initialize mapper
  initializeAutoMapper();

  // Create server
  const server: Express = express();

  // Set custom headers
  server.use((request, response, next) => {
    response.append("X-Robots-Tag", "none");
    response.removeHeader("X-Powered-By");
    next();
  });

  // CORS
  server.use(cors());

  // Enable Routing
  Routing.useExpressServer(server, {
    controllers: [`${__dirname}/controllers/*.ts`]
  });

  // Start server
  const port = Number(process.env.SERVER_PORT) || 8000;
  server.listen(port, () => {
    console.log(`Server is running on ${port}`);
  });
  
  // Swagger docs
  createSwaggerDocs(server);

  await db;
}

// Start
init().catch((err) => {
  console.error(err);
});