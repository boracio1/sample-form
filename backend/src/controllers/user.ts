import "automapper-ts";

import { JsonController, Post, Body, OnUndefined, UploadedFile, BadRequestError, UseBefore, Req } from "routing-controllers";
import { getRepository } from "typeorm";
import * as Multer from "multer";

import { User } from "~/entities/user";
import { UserCreateModel } from "~/models/user";
import { UploadFileHelper } from "~/helpers/upload";
import { Asset } from "~/entities/asset";
import { AssestType } from "~/entities/asset";

const multer = Multer(UploadFileHelper.options);

@JsonController("/users")
export class UserController {
  /**
   * @swagger
   * /users:
   *  post:
   *   summary: Register a new applicant
   *   tags:
   *    - Users
   *   consumes:
   *    - multipart/form-data
   *   parameters:
   *    - name: fullName
   *      description: Full name of a new user to create
   *      in: formData
   *      required: true
   *      type: string
   *    - name: email
   *      description: email of a new user to create
   *      in: formData
   *      required: true
   *      type: string
   *    - name: picture
   *      in: formData
   *      type: file
   *      required: true
   *      description: Picture to be uploaded
   *    - name: pdf
   *      in: formData
   *      type: file
   *      required: true
   *      description: PDF to be uploaded
   *   responses:
   *    204:
   *     description: The user has been created
   *    400:
   *     $ref: '#/responses/BadRequest'
   */
  @Post("/")
  @OnUndefined(204)
  @UseBefore(multer.fields([{ name: "picture", maxCount: 1 }, { name: "pdf", maxCount: 1 }]))
  public async createOne(
    @Body({ validate: true }) model: UserCreateModel,
    @Req() req
  ) {
    const picture = req.files["picture"][0];
    const pdf = req.files["pdf"][0];

    // Mapp to entity
    let user: User = automapper.map(UserCreateModel.key, User.key, model);
    // Create user
    await
      getRepository(User)
        .insert(user);

    // if (!picture || !pdf)
    if (!picture)
      throw new BadRequestError("File is too large");
    // Upload assets
    const promisePicture = UploadFileHelper.uploadAsset(picture.buffer, user.id, picture.originalname);
    const promisePdf = UploadFileHelper.uploadAsset(pdf.buffer, user.id, pdf.originalname);

    user.picture = await
      getRepository(Asset)
        .save({
          url: await promisePicture,
          type: AssestType.Picture
        } as Asset);
    user.pdf = await
      getRepository(Asset)
        .save({
          url: await promisePdf,
          type: AssestType.PDF
        } as Asset);
    // Update user data
    await
      getRepository(User)
        .save(user);
  }
}