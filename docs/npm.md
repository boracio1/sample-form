# NPM

As the project is that of NodeJS, we chose to use NPM as a package manager.

## Commands
- `start`:  
 Runs optimized for production.
- `dev`:   
 Runs the devserver.
- `docker-dev`:  
 Runs the devserver, optimized for running within a docker container. (note: uses polling)
- `typeorm`:  
 TypeORM alias to be used as alternative to a global install of TypeORM.
- `tslint`:  
 Runs the linting tool.

 ## TypeORM

How to use the TypeORM CLI:

```
npm run typeorm -- <args>
```

For example:
```
npm run typeorm -- migration:generate -n [name]
npm run typeorm -- migration:run
npm run typeorm -- migration:revert
```