# Docker
This project runs in a containerized environment. To manage the containers of the project, we use docker.

## Containers
### - API -
A container based on `node:10.7-alpine` with the purpose of hosting the API endpoint. 

This container is configured in the `Dockerfile` and `docker-compose.yml` files

### - MySQL -
The database container for storing the API data. For the container to have its data persist after restarts, a volume is mounted on the `.mysql` folder. Thus clearing the `.mysql` folder means removing the database.  Of course this means the `.mysql` should never be in any kind of source control.

All settings are configured are in the `docker-compose.yml` file, it's currently based on `mysql:5.7`.

## Useful commands
- `docker-compose up api`:  
  This will start all containers in a detached mode, except for the `api` container, allowing easy and fast restarts of the `api` container.
- `docker-compose up --build api api`:  
  Similar as the above, but will rebuild the container. Useful, e.g., if the package.json has been changed.
- `docker-compose up --build front`:  
  Similar as the above, but will rebuild the frontend container. Needs to be done after frontend code has been changed.
- `docker-compose down`:  
  This will stop all containers, even the detached ones.
- `docker volume rm $(docker volume ls -q)`:  
  Removes all docker volumes. (Use with care)
- `docker stop $(docker ps -aq)`:  
  Stops all running containers.
- `docker rm $(docker ps -aq)`:   
  Removes all stopped containers. (Use with care)