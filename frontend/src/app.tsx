import * as React from "react";
import * as $ from "jquery";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { createBrowserHistory } from "history";

import { Button } from "~/components/button";
import { Form } from "~/components/form";
import { Upload } from "~/components/upload";
import { HttpHelper } from "~/helpers/http";
import { GenericHelper } from "~/helpers/generic";

interface IRegisterState {
  userData: IUserData;
  submit?: boolean;
  success?: boolean;
  error?: string;
}

interface IUserData {
  fullName?: string;
  email?: string;
  picture?: File;
  pdf?: File;
}

export class App extends React.Component<RouteComponentProps<{}>, IRegisterState> {
  public static history = createBrowserHistory();
  public state: IRegisterState = {
    userData: {}
  };
  private registerForm: Form;

  private onClose = () => {
    $(".application-form").hide();
  }
  private onOpen = () => {
    $(".application-form").show();
  }

  private textChange(field: keyof IUserData) {
    return (event: React.ChangeEvent<HTMLElement & { value: string }>) => {
      const value: string = event.target.value;
      const userData = Object.assign(this.state.userData, { [field]: value });
      this.setState({ userData });
    };
  }

  private binaryChange(field: keyof IUserData) {
    return (file: File) => {
      const userData = Object.assign(this.state.userData, { [field]: file });
      this.setState({ userData });
    };
  }

  private onSubmit = () => {
    const { userData } = this.state;
    // Prevent submit while request is running
    if (this.state.submit || !this.registerForm.valid()) return;
    // hide submit button
    $(".btn-submit-form").hide();
    // Create new user
    this.setState({ submit: true }, () => {
      HttpHelper.createUser(userData)
        .then((response) => {
          if ( response === 204 ) {
            this.setState({ submit: false, success: true, error: undefined });
            $(".btn-submit-form").show();
          } else {
            this.setState({ submit: false, error: "Something went wrong. Please try again later!" });
            $(".btn-submit-form").show();
          }
        })
        .catch((reason) => {
          console.log(reason);
          const error = reason;
          this.setState({ submit: false, error });
          $(".btn-submit-form").show();
        });
    });
  }

  public componentDidUpdate() {
    const { success, error } = this.state;
    if (success) {
      setTimeout(() => {    
        this.setState({
          success: false
        });
      }, 10 * 1000);
    } else if (error) {
      setTimeout(() => {
        this.setState({
          error: undefined
        });
      }, 10 * 1000);
    }
  }

  render() {
    const { error, success } = this.state;
    const errorBlock = error ? ( 
      <p className="error-message">{error}</p>
    ) : (
      undefined
    );    
    const successBLock = success ? ( 
      <p className="success">Thank you fo your application.<br /> We wil contact you soon!</p>
    ) : (
      undefined
    );
    return (
      <div className="App">
        <Form className="application-form" ref={(ref) => this.registerForm = ref} onSubmit={GenericHelper.formPreventDefault}>
          <h2>
            Submit your application
              <Button
              className="btn-close-form"
              onClick={this.onClose}
            >
              {"x"}
            </Button>
          </h2>
          {errorBlock}
          {successBLock}
          <div className="form-group">
            <label htmlFor="fullName"><b>Full name</b> (required)</label>
            <input
              type="text"
              name="fullName"
              placeholder="John Smith"
              onChange={this.textChange("fullName")}
              minLength={3}
              required={true}
              data-msg-required="Please enter your full name."
            />
            <div className="invalid-feedback" data-error="fullName" />
          </div>
          <div className="form-group">
            <label htmlFor="name"><b>Email</b> (required)</label>
            <input
              type="email"
              name="email"
              placeholder="john@pipedrive.com"
              onChange={this.textChange("email")}
              minLength={3}
              required={true}
              data-msg-required="Please enter your valid email."
            />
            <div className="invalid-feedback" data-error="email" />
          </div>
          <div className="form-group">
            <label htmlFor="name"><b>Photo</b> (required)</label>
            <div className="btn-upload-container">
              <Upload
                id="photo"
                onChange={this.binaryChange("picture")}
                accept="image/jpeg, image/png"
                message="Please upload your photo."
              />
            </div>
          </div>
          <div className="form-group">
            <label htmlFor="name"><b>Resume/CV</b> (required)</label>
            <div className="btn-upload-container">
              <Upload
                id="pdf"
                onChange={this.binaryChange("pdf")}
                accept="application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                message="Please enter your resume/CV."
              />
            </div>
          </div>
          <div className="submit-container">
            <Button
              className="btn-submit-form"
              onClick={this.onSubmit}
            >
              {"Submit application"}
            </Button>
          </div>
        </Form>
        <Button
          className="btn-open-form"
          onClick={this.onOpen}
        >
          {"Apply now"}
        </Button>
      </div>
    );
  }
}

export const AppComponent = withRouter(App);