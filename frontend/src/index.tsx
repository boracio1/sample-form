/* jQuery validation */
import "jquery-validation";
import "jquery-validation/dist/additional-methods.js";
/*Styling*/
import "../style/main.css";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Router } from "react-router";

import { App, AppComponent } from "./app";

// Start rendering application
ReactDOM.render(
  <Router history={App.history}><AppComponent /></Router>, document.getElementById("root")
);