export class GenericHelper {
  public static formPreventDefault = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  }
}

export type Dictionary<T> = { [key: string]: T };

export type Writeable<T> = {
  [P in keyof T]?: T[P]
};