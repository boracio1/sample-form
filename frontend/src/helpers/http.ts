import axios, { AxiosInstance } from "axios";
import { UserCreateModel } from "~/models/user";


export class HttpHelper {
  private static request: AxiosInstance = axios.create({
    baseURL: "http://localhost:8000"
  });

  public static createUser(user: UserCreateModel): Promise<string | number> {
    const data = new FormData();
    data.append("fullName", user.fullName);
    data.append("email", user.email);
    data.append("picture", user.picture);
    data.append("pdf", user.pdf);
    return HttpHelper.request
      .post<UserCreateModel>("/users", data)
      .then((response) => response.status)
      .catch((error) => error);
  }
}