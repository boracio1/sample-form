declare interface JQuery<TElement = HTMLElement> extends Iterable<TElement> {
  validate: (options?: any) => JQueryValidation;
  valid: () => boolean;
}

declare interface JQueryValidation {
  resetForm: () => void;
}

declare interface JQueryStatic {
  validator: { addMethod: (...params: any[]) => void, messages: string };
}