import * as React from "react";
import * as $ from "jquery";
import { Writeable, Dictionary } from "~/helpers/generic";

interface IFormProps {
  errorElement?: string;
  errorContainer?: string;
  useErrorContainer?: boolean;
  rules?: Dictionary<{}>;
  id?: string;
  className?: string;
  children?: React.ReactNode;
  onSubmit?: React.FormEventHandler<HTMLFormElement>;
  onError?: () => void;
}

/**
 * Simple form component with validation enabled
 */
export class Form extends React.Component<IFormProps> {
  public $ref: JQuery<HTMLFormElement>;
  private validator: JQueryValidation;

  private setRef = (ref: HTMLFormElement) => {
    const $ref = $(ref);
    if (!ref || !$ref) return;

    const { errorContainer, errorElement, rules, useErrorContainer, onError } = this.props;

    const validateOptions = {
      rules: rules,
      errorClass: "is-invalid",
      errorContainer: `#${errorContainer}`,
      errorElement: errorElement,
      errorPlacement: (error: JQuery, element: HTMLElement) => {
        const name = $(element).attr("name");
        const $container = $(`[data-error="${name}"]`);
        error.appendTo($container);
        if (useErrorContainer) {
          error.appendTo($(`#${errorContainer}`));
        }
      },
      invalidHandler: () => {
        if (onError) {
          onError();
        }
      },
      highlight: (element: HTMLInputElement) => {
        $(element).addClass("is-invalid");
        if ($(element).hasClass("form-check-input"))
          $(element).parents(".form-check-label").addClass("has-danger");
        $(element).parents(".form-group").addClass("has-danger");
      },
      unhighlight: (element: HTMLInputElement) => {
        $(element).removeClass("is-invalid");
        if ($(element).hasClass("form-check-input"))
          $(element).parents(".form-check-label").removeClass("has-danger");
        $(element).parents(".form-group").removeClass("has-danger");
      }
    };
    this.validator = $ref.validate(validateOptions);
    this.$ref = $ref;
  }

  public valid = () => {
    $("input[type='text'], textarea").each(function () {
      const $element = $(this);
      const value = $element.val() as string;
      $element.val(value.trim());
      $element.change();
    });
    return this.$ref.valid();
  }
  public componentWillUnmount() {
    if (this.validator)
      this.validator.resetForm();
  }

  public render() {
    // Filter some props as they don't exist on HTMLFormElement
    const props: Writeable<IFormProps> = Object.assign({}, this.props);
    delete props.rules;
    delete props.errorContainer;
    delete props.errorElement;
    delete props.useErrorContainer;
    return (
      <form
        ref={this.setRef}
        role="form"
        {...props}
      />
    );
  }
}