import * as React from "react";
import * as $ from "jquery";

import { Button } from "./button";

interface IUploadProps {
  id: string;
  accept?: string;
  onChange: (file: File) => void;
  message?: string;
}

interface IUploadState {
  name: string;
}

const MAX_SIZE = 20e6;

export class Upload extends React.Component<IUploadProps, IUploadState> {
  public state: IUploadState = {
    name: undefined
  };

  private $input: JQuery<HTMLInputElement>;

  private openInput = () => {
    if (this.$input)
      this.$input.click();
  }

  private readFile(file: File) {
    if (file && file.name) {
      // Pass file name into state
      this.setState({ name: file.name });
    }
  }

  private onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files[0];
    if (event.target.className.indexOf("invalid") > 0) {
      $(event.target).valid();
    }
    this.props.onChange(file);
    this.readFile(file);
  }

  private onRemove = () => {
    if (this.$input)
      this.$input.val(undefined);
      this.setState({ name: undefined });
    this.props.onChange(undefined);
  }

  public componentDidMount() {
    $.validator.addMethod("filesize", function (_value: string, element: HTMLInputElement, param: number) {
      return this.optional(element) || (element.files[0].size <= param);
    }, "File size must be less than 5MB");

    $.extend($.validator.messages, {
      accept: "Provided file type is not valid"
    });
  }

  public render() {
    const { id, accept, message } = this.props;
    const { name } = this.state;

    const nameBlock = name ? ( 
      <p>File Name: <b>{name}</b></p>
    ) : (
      undefined
    );

    const buttonsBlock = name ? (
      <Button
        className="btn-upload btn-danger"
        onClick={this.onRemove}
      >
        {" Remove file"}
      </Button>
    ) : (
      <span onClick={this.openInput}>
        <span className="btn-upload">Choose file</span>
      </span>
    );
    return (
      <div>
        {buttonsBlock}
        <input
          type="file"
          className="form-control"
          style={{ opacity: 0, height: 0, position: "absolute" }}
          name={id}
          id={id}
          ref={(ref) => this.$input = $(ref)}
          accept={accept}
          onChange={this.onChange}
          required={true}
          data-rule-filesize={MAX_SIZE}
          data-msg-required={message}
        />
        {nameBlock}
        <div className="invalid-feedback" data-error={id} />
      </div>
    );
  }
}