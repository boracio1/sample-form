import * as React from "react";

export interface IButtonProps {
  /** OnClick callback */
  onClick: () => void;
  /** Child elements */
  children?: React.ReactChild | React.ReactChild[] | string;
  /** Optional class names */
  className?: string;
  /** Optional styling */
  style?: React.CSSProperties;
  /** Button ID */
  id?: string;
  /** Optional title */
  title?: string;
}

/**
 * Styled button
 */
export class Button extends React.Component<IButtonProps> {
  /**
   * OnClick handler
   */
  public click = () => {
    this.props.onClick();
  }

  /**
   * React render
   */
  public render() {
    const { className, children, style, id, title } = this.props;

    return (
      <button
        className={className}
        onClick={this.click}
        style={style}
        id={id}
        title={title}
      >
        {children}
      </button>
    );
  }
}