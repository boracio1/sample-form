# Sample Form
Sample Job Application Form

To run project, use the following command;
```
docker-compose up api
```
It will start the backend and frontend instances, frontend is reachable on `localhost:3000`

## Backend 
Backend is based on NodeJs and Typescript. 

### Pages with more info
- [NPM](./docs/npm.md)
- [Docker](./docs/docker.md)
- [typeorm](http://typeorm.io/)
- [routing-controllers](https://github.com/typestack/routing-controllers)
- [automapper-ts](https://github.com/loedeman/AutoMapper)

### Swagger
This project uses Swagger from the documentation of its API endpoints, when the project instance is running it can be found under the `/api-docs` path, e.g.;
```
http://localhost:8000/api-docs/

```
### My SQL Database
This project is using MySQL database.

### Files
For the proof of concept are files currently uploaded only locally to tmp folder.

## Frontend
Frontend is based on ReactJs and Typescript. 